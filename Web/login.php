<?php

include_once 'config.php';

if (isset($_POST['username'])) {
    if ($_POST['username'] == '' || $_POST['password'] == '') {
        die("Username or password was left empty.");
    }

    if ($auth->login($_POST['username'], $_POST['password'])) {
        header('Location: dash.php');
    } else {
        die("Your input does not match any records.");
    }
}

?>
