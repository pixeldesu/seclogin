<?php

include_once '../vendor/autoload.php';
include_once 'config.php';

$router = new \pixeldesu\seclogin\Router();
$router->perform();

if (true === $_SESSION['logged_in']) {
    header('Location: dash.php');
}
