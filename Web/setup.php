<?php

include_once 'config.php';

// As soon as you visit this script, we set up a simple username/password table
$db->createUserTable();

if (isset($_POST['username'])) {
    if ($_POST['username'] == '' || $_POST['password'] == '') {
        die("Username or password was left empty.");
    }

    // Let's create the user with the credentials specified
    if($db->createUser($_POST['username'], $_POST['password'])) {
        header('Location: index.php');
    } else {
        die('Something went wrong while creating the user!');
    }
}

?>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Set up</title>
</head>
<body>

<h1>Set up</h1>
<p>You basically just create an user here.</p>

<form method="post">
    <div><label>Name: <input type="text" name="username"></label></div>
    <div><label>Password: <input type="password" name="password"></label></div>
    <input type="submit">
</form>

</body>
</html>