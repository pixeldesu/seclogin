<?php

namespace pixeldesu\seclogin\Controller;

class AbstractController
{
    public function render() {
        echo file_get_contents('./views/' . $this->getClassName() . '.html');
    }

    public function getClassName() {
        return strtolower(end(explode('\\', get_class($this))));
    }
}
