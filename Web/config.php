<?php

if (false === file_exists(__DIR__ . '/config.json')) {
    throw new \Exception('Please create a config.json with your database credentials');
} else {
    $config = file_get_contents(__DIR__ . '/config.json');
    $config = json_decode($config, true);
}

// Database connection and authentication!
$db = new \pixeldesu\seclogin\DatabaseConnection(...array_values($config));
$auth = new \pixeldesu\seclogin\Auth($db);
