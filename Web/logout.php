<?php

include_once 'config.php';

if ($auth->logout()) {
    header('Location: index.php');
} else {
    die('How about you log in first?');
}