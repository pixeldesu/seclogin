<?php

namespace pixeldesu\seclogin;

class Auth
{
    private $db;

    /**
     * Auth constructor.
     * @param $db
     */
    function __construct($db)
    {
        session_start();

        if (false === isset($_SESSION['logged_in'])) {
            $_SESSION['logged_in'] = false;
        }

        $this->db = $db;
    }

    /**
     * @param $username
     * @param $password
     * @return bool
     */
    function login($username, $password)
    {
        $user = $this->db->getUser($username);

        if (($user['username'] === $username) && (hash_equals($user['password'],
                crypt($password, $user['password'])))
        ) {
            $_SESSION['logged_in'] = true;
            $_SESSION['username'] = $user['username'];

            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    function logout()
    {
        if (false === $_SESSION['logged_in']) {
            return false;
        } else {
            $_SESSION['logged_in'] = false;
            $_SESSION['username'] = '';
            return true;
        }
    }
}
