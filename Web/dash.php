<?php

include_once 'config.php';

if (false === $_SESSION['logged_in']) {
    header('Location: index.php');
}

?>

<!DOCTYPE html>
<html>
<head>
    <title>Dashboard</title>
</head>
<body>
<p>Just some text here, telling you that you are logged in as <?php echo $_SESSION['username'] ?>!</p>

<p><a href="logout.php">Log out</a></p>
</body>
</html>
