<?php
/**
 * Created by PhpStorm.
 * User: andreas
 * Date: 11.04.17
 * Time: 15:15
 */

namespace pixeldesu\seclogin;


class Router
{
    public function perform() {
        switch ($_SERVER['REQUEST_URI']) {
            case '/':
                $action = new \pixeldesu\seclogin\Controller\Index();
                $action->render();
            case '/login':
                $action = new \pixeldesu\seclogin\Controller\Login();
                $action->render();
        }
    }
}
