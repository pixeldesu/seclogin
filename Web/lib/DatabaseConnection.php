<?php

namespace pixeldesu\seclogin;

class DatabaseConnection
{

    private $sql;

    private $salt;

    /**
     * DatabaseConnection constructor.
     * @param $host
     * @param $user
     * @param $password
     * @param $database
     */
    function __construct($host, $user, $password, $database)
    {
        $this->sql = new \mysqli($host, $user, $password, $database);
        $this->salt = random_bytes(20);
    }

    /**
     * @throws Exception
     */
    function createUserTable()
    {
        $userTableQuery = <<<SQL
      CREATE TABLE IF NOT EXISTS users (
        id INT NOT NULL AUTO_INCREMENT,
        username VARCHAR(16) NOT NULL UNIQUE,
        password TEXT NOT NULL,
        PRIMARY KEY (`id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
SQL;

        if (false === $this->sql->query($userTableQuery)) {
            throw new \Exception('Something went wrong while creating the user table!');
        }
    }

    /**
     * @param $username
     * @param $password
     * @throws Exception
     * @return bool
     */
    function createUser($username, $password)
    {
        $userQuery = "INSERT INTO users (username, password) VALUES ('" . $this->sql->real_escape_string($username) . "','" . $this->sql->real_escape_string(crypt($password,
                $this->salt)) . "');";

        if (false === $this->sql->query($userQuery)) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * @param $username
     * @return array
     * @throws \Exception
     */
    function getUser($username)
    {
        $userGetQuery = "SELECT username, password FROM users WHERE username = \"" . $this->sql->real_escape_string($username) . "\";";

        $result = $this->sql->query($userGetQuery);

        if (false === $result) {
            throw new \Exception('Something went wrong while fetching the user!');
        } else {
            return $result->fetch_assoc();
        }
    }

}
